<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '1337m4st3r' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'G77]{}p|U8vtNC=2-_eSr~?8N<Y7+Cy!VDl|c1$TI-E)/Z{8>&_5hSyb/(Vr^u*=' );
define( 'SECURE_AUTH_KEY',  'n674ZdMAgWtqZU*g!xFI@murd#{yxbW7O2T#ev C8RZUp4VzW>FTM!F;RSDK35j4' );
define( 'LOGGED_IN_KEY',    'i2+hc^e9/hB?QC&obHI-uwym1W*fft;LjJa~$|cEkQy4YQan)}SokHY`s(M<N*j5' );
define( 'NONCE_KEY',        '*jnt3(VgW@AS_}~fmwf.1:r1HEvX{-WFC;R(r4ERAd05XuPQZ@O}A@Q?$Qt/;CtI' );
define( 'AUTH_SALT',        '{,~VokOW%XlY%STH8ehgetl^f5/E>H*=kf9hRGfZ<%r}T+CB$B76Y}eCvoy-F3/9' );
define( 'SECURE_AUTH_SALT', '.4ul=}gb$!M-p%Mtl2 @bX$`|$Lt}$H;[Qia,PsV`yfaxn]*&;Eoej+q?zyd.73v' );
define( 'LOGGED_IN_SALT',   'k`l%,nO=l4!r;E%vB U>M+gw75CR7&f;EbY(meUUZx]ys`^7(/;vM`-KxEbSg}ZC' );
define( 'NONCE_SALT',       'e=+Py9IyG<C1[;[,_0ftB[o.Ot)JP%88[Jkc6G]5NkqcG_Q7y*9rk~ }sYc9d@p=' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'cliweb';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
