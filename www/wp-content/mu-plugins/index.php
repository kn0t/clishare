<?php
/*///////////////////////////////////////////////////////////////////
 * Plugin Name: The silence of the Mu
 * Version: 1.0
 * Plugin URI: https://eligent.group
 * Description: The quieter you become; the more you're able to hear.
 * Author: Filippo kn0t Teodori
 * Author URI: https://ptroy.io/
 * Requires at least: 4.0
 * Tested up to: 5.3
 */
/////////////////////////////////////////////////////////////////////
// The quieter you become; the more you're able to hear.
?>