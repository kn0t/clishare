<?php

namespace Clishare;

class APIs extends Routes {

	private $endpoints = [
		"api/v1/bubba" 	=>	"bubbaController",
		"api/v1/ray"	=>	"rayController"
	];

	public function __construct($endpoint) {
		if (isset($this->endpoints[$endpoint])) {
			$controllerName 	=	$this->endpoints[$endpoint];
			if (method_exists($this,$controllerName)) {
				$this->{$controllerName}();
			}
		}
	}

	private function bubbaController() {

		if (!$this->isValidPublicIP($_SERVER['REMOTE_ADDR'])) {
			$this->jsonErrorReturn(["error"	=>	"Request blocked."]);
		}

		$phpInput 	=	json_decode(file_get_contents("php://input"));
		if (is_object($phpInput)) {
			if (!isset($phpInput->storage_class)) {
				$document['storage_class']	=	"regular";
			} else {
				$document['storage_class']	=	$phpInput->storage_class;
			}
			$validation =	$this->validateInput($phpInput->data);
			if (isset($validation["error"])) {
				$this->jsonErrorReturn($validation);
			}

			$document['creation_time'] 	=	time();
			$document['data'] 			=	$phpInput->data;
			$UID 						=	$this->generateRandomString();
			$document['UID'] 			=	$UID;
			$document['remote_addr'] 	=	$_SERVER['REMOTE_ADDR'];

			$mongo 	= new Mongo();

			if(!isset($phpInput->auth_token)) {
				$mongo->createGuestEntry($document);
			} else {
				$jwt = new JWT();
				if (is_string($jwt->isValidToken($phpInput->auth_token))) {
					$user_id 	=	$jwt->getUserID($phpInput->auth_token);
					$mongo->createUserEntry($user_id, $document);
				}
			}

			$this->jsonReturn(["url" => "https://clishare.eligent.group/$UID"]);

		} else {
			$this->jsonErrorReturn(["error"	=>	"No data passed."]);
		}
	}

	private function rayController() {
		$phpInput 	=	json_decode(file_get_contents("php://input"));
		if (is_object($phpInput)) {
			if (!$this->isValidPublicIP($_SERVER['REMOTE_ADDR'])) {
				$this->jsonErrorReturn(["error"	=>	"Request blocked."]);
			}

			if (isset($phpInput->extend)) {
				$extend 	=	$phpInput->extend;
			} else { $extend = false; }

			if (isset($phpInput->user) && isset($phpInput->password)) {
				$this::$auth_module->APIlogin($phpInput->user, $phpInput->password, $extend);
			}

			if (isset($phpInput->token)) {
				$jwt 	=	new JWT($extend);
				$jwt->renewToken($phpInput->token);
			}
		} else {
			$this->jsonErrorReturn(["error"	=>	"No data passed."]);
		}
	}
}

?>