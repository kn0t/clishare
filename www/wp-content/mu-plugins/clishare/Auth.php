<?php

namespace Clishare;

class Auth extends Core {

	protected $auth_status = false;
	
	public function __construct() {

		if (is_user_logged_in()) {
			$this->auth_status = true;
		}

		// Main auth controller ////////////////////
		if (isset($_POST['login_email']) && isset($_POST['login_password'])) {
			$this->auth_status = $this->authentication(1,2);
			if ($this->auth_status === true) {
				header("Location: /");
			} else if ($this->auth_status !== true or is_string($this->auth_status)) {
				add_filter('timber_context', [$this, 'returnAuthError']);
			}
		}

		// Main register controller ///////////////
		if (isset($_POST['signup_email']) && isset($_POST['signup_password']) && isset($_POST['signup_username'])) {
			$signup_status 	=	$this->makeClishareUser($_POST['signup_email'], $_POST['signup_password'], $_POST['signup_username']);
			if($signup_status === true) {
				header("Location: /?success");
			} else {
				add_filter('timber_context', [$this, 'returnSignupError']);
			}
		}

	}

	private function authentication($email, $password) {
		$creds 	=	[
			"user_login" 	=> $_POST['login_email'], 
			"user_password"	=> $_POST['login_password'],
			"remember"		=> true
		];

		$user 	=	wp_signon($creds);
		if (is_wp_error($user)) {
			return $user->get_error_message();
		} else if (isset($user->ID)) {
			return true;
		}
	}

	public function returnAuthError($context) {
		$context["auth_error"]	=	$this->auth_status;
		return $context;
	}

	public function returnSignupError($context) {
		$context["auth_error"]	=	$this->signup_status;
		return $context;
	}

	public function makeClishareUser($email, $password, $username) {
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return "Invalid email address.";
		} 
		$user = wp_create_user($username, $password, $email);
		if (is_wp_error($user)) {
			return $user->get_error_message();
		} else if (is_int($user)) {
			return true;
		}
	}

	public function APIlogin($user, $password, $extend) {
		$user 	=	wp_authenticate($user,$password);
		if (is_wp_error($user)) {
			return $user->get_error_message();
		} else if (isset($user->ID)) {
			$jwt 	=	new JWT($extend);
			$auth_response 	=	$jwt->makeClishareToken($user, $extend);
			$this->jsonReturn($auth_response);
		}
	}

	public function getUserShortTermHash($user_id) {
		$user 	=	get_user_by('ID',$user_id);
		return md5($user->user_email."short");
	}

}