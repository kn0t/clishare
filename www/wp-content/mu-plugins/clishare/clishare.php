<?php

namespace Clishare;

Class Core {

	static public $url_characters 	= 	'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	public $is_production;
	public $production_hostname 	= 	"clishare.eligent.io";
	public $inner_context 			= 	[];
	public $auth_error 				=	false;
	public $mongoDB 				=	false;
	public $allowed_encodings		=	["ASCII", "UTF-8"];
	public $character_limit 		=	84000;
	public static $auth_module 		=	false;

	public function __construct() {

		// Populate properties ////////////////////////////
		
		if (strpos($_SERVER['HTTP_HOST'],$this->production_hostname) !== false ||
			strpos($_SERVER['SERVER_NAME'],$this->production_hostname) !== false) {
			$this->is_production = true;
		} else {
			$this->is_production = false;
		}
		
		// Scheduling actions /////////////////////////////
		add_action('init', [$this, 'initEmails'],		1);
		add_action('init', [$this, 'initAjax'], 		6);
		add_action('init', [$this, 'initAuth'], 		7);
		add_action('init', [$this, 'initRenderer'], 	8);
		add_action('init', [$this, 'initRoutes'], 		9);

	}

	// Instances //////////////////////////////////////////

	public function initAjax() {	
		new Ajax();
	}

	public function initRenderer() {	
		new Renderer();
	}

	public function initRoutes() {	
		new Routes();
	}

	public function initAuth() {
		$this::$auth_module 	=	new Auth();
	}

	public function initEmails() {
		new Emails();
	}

	// Core functions ////////////////////////////////////

	public function d($var, $kill = false) {
		print("<pre><code>");
		var_dump($var);
		print("</pre></code>");
		if ($kill) {die();}
	}

	protected function matchCurrentRequest($route) {
		if ($this->current_route === $route) {
			return true;
		}
		return false;
	}

	protected function render($view, $context = []) {
		Enqueuer::load("core"); // Always load the core first.
		Enqueuer::load($view);		
		$renderer = new Renderer;
		$renderer->render($view, $context);
	}

	protected function isMultibyte($value) {
		return mb_strlen($value,'utf-8') < strlen($value);
	}

	public function jsonErrorReturn($returnArray) {
		http_response_code(400);
		$returnValue 	=	json_encode($returnArray);
		header('Content-Type: application/json');
		die($returnValue);
	}

	public function jsonReturn($returnArray) {
		http_response_code(200);
		$returnValue 	=	json_encode($returnArray);
		header('Content-Type: application/json');
		die($returnValue);
	}

	public function validateInput($input) {
		if (mb_strlen($input) > $this->character_limit) {
			return ["error" 	=>	"Input exceeds character limit."];
		}
		if (!mb_detect_encoding($input) || !in_array(mb_detect_encoding($input), $this->allowed_encodings)) {
			return ["error" 	=>	"Input isn't UTF-8."];
		}
		return true;
	}

	public function generateRandomString($length = 10) {
	    $charactersLength = strlen($this::$url_characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $this::$url_characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	protected function renderRawPaste($paste_data) {
		http_response_code(200);
		header('Content-Type: text/plain');
		die($paste_data);
	}

	public function isValidPublicIP($ip) {
		if (filter_var($ip, FILTER_VALIDATE_IP)) {
			return true;
		} else {
			return false;
		}
	}

	public function isVolatile($program) {
		if (isset($program->storage_class)) {
			if ($program->storage_class == "volatile") {
				return true;
			}
		}
		return false;
	}

}