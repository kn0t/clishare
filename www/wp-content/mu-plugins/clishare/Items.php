<?php

namespace Clishare;

Class Items {

	private static $items 	= [
		// "dashboard"	=>	[
		// 	"left"		=>	[
		// 		"ENCKEY"	=>	[
		// 			"title"		=>	"Encryption Key",
		// 			"classes"	=>	"",
		// 			"includes"	=>	[
		// 				"parts/dashboard/includes/enckey_field.twig"
		// 			]
		// 		],
		// 		"ENTRIES"	=>	[
		// 			"title"		=>	"Passwords",
		// 			"classes"	=>	"enc_disabled inline_title ptr0y_entries_card hidden",
		// 			"includes"	=>	[
		// 				"parts/dashboard/includes/passwords_add_button.twig",
		// 				"parts/dashboard/includes/passwords_entries.twig"
		// 			]
		// 		]
		// 	],
		// 	"core"		=>	[
		// 		"REST"		=>	[
		// 			"classes"	=>	"active",
		// 			"includes"	=>	[
		// 				"parts/dashboard/includes/rest.twig"
		// 			],
		// 		],
		// 		"ADD_ENTRY" =>	[
		// 			"title"		=>	"Add new password entry",
		// 			"classes"	=>	"hidden",
		// 			"includes"	=>	[
		// 				"parts/dashboard/includes/add_entry_form.twig"
		// 			],
		// 		],
		// 		"PASSWORD_OUTPUT"=>	[
		// 			"classes"	=>	"hidden",
		// 			"includes"	=>	[
		// 				"parts/dashboard/includes/ptr0y_password_output.twig"
		// 			],
		// 		],
		// 	],	
		// ],
	];

	public static function getItems() {
		return self::$items;
	}

}