<?php

namespace Clishare;

use Ahc\Jwt\JWT as Core_JWT;

class JWT extends Core {

	public static $JWT 		=	false;
	public static $JWT_KEY	=	"testScaraBottoLo1381";
	
	public function __construct($extend = false) {
		require_once JWT_AUTOLOAD;

		if ($extend === false) {
			$exp 	=	900;
		} else {
			$exp 	=	176400;
		}

		$this::$JWT = new Core_JWT($this::$JWT_KEY, 'HS256', $exp, 10);
	}

	public function makeClishareToken($user,$extend = false) {
		if (is_object($user)) {
			$term = "short";
			if ($extend === true) { $term = "long"; }
			$token 	=	$this::$JWT->encode([
				'user_id'	=>	$user->ID,
				'user_hash'	=>	md5($user->user_email.$term)
			]);
			return ["token" => $token];
		}
		return false;
	}

	public function renewToken($token) {
		$payload 	=	$this->isValidToken($token);
		$new_token 	=	$this->makeClishareToken(get_user_by('ID',$payload["user_id"]),false);
		$this->jsonReturn(["token" => $token]);
	}

	public function isValidToken($token) {
		try {
			$payload = $this::$JWT->decode($token);
		} catch(\Exception $e) {
			$this->jsonErrorReturn(["error" => "Invalid token."]);
		}
		if (!isset($payload["user_id"]) || !isset($payload["user_hash"])) {
			$this->jsonErrorReturn(["error" => "Invalid token."]);
		}
		$user_hash = $this::$auth_module->getUserShortTermHash($payload["user_id"]);
		if ($payload["user_hash"] != $user_hash) {
			$this->jsonErrorReturn(["error" => "Invalid token."]);
		}
		return $token;
	}

	public function getUserID($token) {
		$payload 	=	$this->isValidToken($token);
		if (isset($payload["user_id"])) {
			return $payload["user_id"];
		}
		return false;
	}
}

?>