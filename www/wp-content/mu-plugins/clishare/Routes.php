<?php

namespace Clishare;

class Routes extends Core {

	private $current_request;
	protected $current_route;
	protected $paste_routes;

	private $routes 	=	[
		// ""			=>	"homeController",
		"login"		=>	"loginController",
		"register"	=>	"registerController",
		// "recovery"	=>	"recoveryController",
	];
	
	public function __construct() {

		$mongo 						= 	new Mongo();
		$this->paste_routes 		=	$mongo->loadRoutes();
		
		$this->current_request		=	ltrim($_SERVER['REQUEST_URI'],"/");
		$this->current_api_request	=	rtrim(explode("&",explode("?",$this->current_request)[0])[0],"/");
		$this->current_route 		=	explode("&",explode("?",explode("/",$this->current_request)[0])[0])[0];

		if ($this->current_route == "api") {
			$APIs 	=	new APIs($this->current_api_request);
		}

		if (isset($this->paste_routes)){
			foreach ($this->paste_routes as $route) {
				if ($this->matchCurrentRequest($route)) {
					$paste_data = $mongo->getPasteData($route);
					if ($paste_data) {
						$this->renderRawPaste($paste_data);
					}
				}
			}
		}

		foreach ($this->routes as $route => $callback) {
			if ($this->matchCurrentRequest($route)) {
				$this->{$callback}();
			}
		}
	}

	// protected function homeController() {
	// 	if (is_user_logged_in()) {
	// 		$this->render('home'); // Change this to dashboard, etc.
	// 	} else {
	// 		wp_redirect("/login");
	// 	}
	// }

	protected function loginController() {
		if (is_user_logged_in()) {
			wp_redirect("/");
		} else {
			$this->render('login', "Login");
		}
	}

	protected function registerController() {
		$this->render('register', "Register");
	}

	// protected function recoveryController() {
	// 	$this->render('recovery', "Password Recovery");
	// }

}

?>