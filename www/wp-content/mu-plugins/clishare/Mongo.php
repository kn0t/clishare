<?php

namespace Clishare;

class Mongo extends Core {
	////////////////////////////////////////////////
	protected $mongo;
	////////////////////////////////////////////////
	static protected $plain_collection 	=	"plain";
	static protected $teams_collection 	=	"teams";
	static protected $users_collection 	=	"users";
	////////////////////////////////////////////////
	static private $mongo_host 			=	"localhost";
	static private $mongo_port 			=	"11499";
	static private $mongo_db 			=	"clishare";
	////////////////////////////////////////////////
	static private $mongo_vm_host 		=	"eligent.group";
	static private $mongo_vm_port 		=	"11499";
	static private $mongo_vm_db 		=	"clishare";
	////////////////////////////////////////////////
	static private $mongo_user 			=	"nello";
	static private $mongo_password 		= 	"M0fvLVfTCHVtCOR8Qk17";
	////////////////////////////////////////////////
	private $mongo_use 		=	false;
	
	public function __construct(){
		if (!$this->mongo) {

			$mongo_user 	=	$this::$mongo_user;
			$mongo_password	=	$this::$mongo_password;

			if ($_SERVER['SERVER_NAME'] == $this->production_hostname) {
				$mongo_host 	=	$this::$mongo_host;
				$mongo_port 	=	$this::$mongo_port;
				$mongo_db 		=	$this::$mongo_db;
			} else {
				$mongo_host 	=	$this::$mongo_vm_host;
				$mongo_port 	=	$this::$mongo_vm_port;
				$mongo_db 		=	$this::$mongo_vm_db;
			}

			$this->mongo_use 	=	$mongo_db;	
			$this->mongo 	= 	new \MongoDB\Driver\Manager("mongodb://$mongo_user:$mongo_password@$mongo_host:$mongo_port/$mongo_db");

			if (!$this->collectionExists($this::$plain_collection)) {
				$this->createCollection($this::$plain_collection);
			}

			if (!$this->collectionExists($this::$teams_collection)) {
				$this->createCollection($this::$teams_collection);
			}

			if (!$this->collectionExists($this::$users_collection)) {
				$this->createCollection($this::$users_collection);
			}
		}
	}

	private function collectionExists($collectionName) {
		$command 	= 	new \MongoDB\Driver\Command(['collStats' => $collectionName]);
		try {
			$cursor 	= 	$this->mongo->executeCommand($this->mongo_use, $command);
	    	$response 	= 	$cursor->toArray()[0];
    	} catch( \MongoDB\Driver\Exception\RuntimeException $e) {
    		return false;
    	}
    	return true;
	}

	private function createCollection($collectionName) {
		$command 	=	new \MongoDB\Driver\Command([
			"create"	=>	$collectionName,
			"capped"	=>	false
		]);
		try {
			$cursor 	= 	$this->mongo->executeCommand($this->mongo_use, $command);
	    	$response 	= 	$cursor->toArray()[0];
    	} catch( \MongoDB\Driver\Exception\RuntimeException $e) {
    		die($e);
    	}
    	return true;
	}

	private function getAllUIDs($collection) {
		$query 	= 	new \MongoDB\Driver\Query([]);
		$rows   = 	$this->mongo->executeQuery("{$this->mongo_use}.{$this::$plain_collection}", $query);
		$UIDs 	=	[];
		foreach ($rows as $entry) {
			if (isset($entry->UID)) {
				$UIDs[]	=	$entry->UID;
			}
		}
		return $UIDs;
	} 

	public function createGuestEntry($document) {

		$command 	= 	new \MongoDB\Driver\Command([
			'insert' 	=> 	$this::$plain_collection, 
			'documents'	=>	[$document]
		]);

		try {
			$cursor 	= 	$this->mongo->executeCommand($this->mongo_use, $command);
	    	$response 	= 	$cursor->toArray()[0];
    	} catch( \MongoDB\Driver\Exception\RuntimeException $e) {
    		die($e);
    	}
    	return true;
	}

	public function createUserEntry($user_id, $document) {

		$user 	=	get_user_by('ID', $user_id);
		if (!isset($user->user_email)) {
			$this->returnErrorJson(["error" => "Request blocked."]);
		} else {
			$document['user_id']	=	$user->ID;
		}

		$command 	= 	new \MongoDB\Driver\Command([
			'insert' 	=> 	$this::$users_collection, 
			'documents'	=>	[$document]
		]);

		try {
			$cursor 	= 	$this->mongo->executeCommand($this->mongo_use, $command);
	    	$response 	= 	$cursor->toArray()[0];
    	} catch( \MongoDB\Driver\Exception\RuntimeException $e) {
    		die($e);
    	}
    	return true;
	}

	public function loadRoutes() {
		$plain_UIDs 	=	$this->getAllUIDs($this::$plain_collection);
		$teams_UIDs		=	$this->getAllUIDs($this::$teams_collection);
		$allRoutes 		=	array_merge($plain_UIDs, $teams_UIDs);
		return $allRoutes;
	}

	public function getPasteData($UID) {
		$query 	= 	new \MongoDB\Driver\Query(['UID' => ['$eq' => "$UID"]]);
		$rows   = 	$this->mongo->executeQuery("{$this->mongo_use}.{$this::$plain_collection}", $query);
		foreach ($rows as $cursor) {

			if ($this->isVolatile($cursor)) {
				$this->removePaste($cursor->UID);
			}

			if (isset($cursor->data)) {
				return $cursor->data;
			}
		}
		return false;
	}

	private function removePaste($UID) {
		if (is_string($UID)) {
			$bulk 	= 	new \MongoDB\Driver\BulkWrite;
			$bulk->delete(['UID' => $UID], ['limit' => 1]);
			$this->mongo->executeBulkWrite("{$this->mongo_use}.{$this::$plain_collection}", $bulk);
		}
	}
}

?>