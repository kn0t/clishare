window.jQuery = window.$ = jQuery;

function ptr0y_activate(view) {
	if (!$("#"+view).hasClass('active')) {
		$(".active").removeClass('active').slideUp(300, function(){
			$("#"+view).slideDown().addClass("active");
		});
	}
}

function ptr0y_addNewEntryView(new_label) {
	new_entry_view	=	"<div class='ptr0y_entry' data-label='"+new_label+"'><b>"+new_label+"</b>Last used: Never</div>";
	$("#ptr0y_entries").prepend(new_entry_view);
}

function ptr0y_hidePassword(password) {
	padding = Math.ceil((password.length - 20)/2);
	slice_1 = password.substr(0,padding)
	slice_2 = password.substr(padding+20,(padding*2)+20)
	return slice_1+"XXXXXXXXXXXXXXXXXXXX"+slice_2;
}

function ptr0y_displayPassword(password) {
	$(".ptr0y_password").text(ptr0y_hidePassword(password));
	$(".ptr0y_password").data("clear", password);
	ptr0y_activate("PASSWORD_OUTPUT")
}

function ptr0y_updateEntry(entry, entry_label) {
	$.ajax({
		'url': ajax_url+"?dbgmode=1",
		'data': {'action': 'get_entry_date', 'entry_label': entry_label},
		'type': 'post',
	})
	.done(function(response){
		if (response != 0 && response != "") {
			new_entry_view	=	"<div class='ptr0y_entry hidden' data-label='"+entry_label+"'><b>"+entry_label+"</b>Last used: "+response+"</div>";
			$("#ptr0y_entries").prepend(new_entry_view);
			$(document).find(".ptr0y_entry.hidden").slideDown();
		}
	});
	$(entry).slideUp(300, function(){
		$(this).remove();
	});
}

function ptr0y_loadPassword(entry_label,encryption_key) {
	$.ajax({
		'url': ajax_url,
		'data': {'action': 'get_password', 'entry_label': entry_label, 'encryption_key': encryption_key},
		'type': 'post',
	})
	.done(function(response){
		if (response != 0 && response != "") {
			ptr0y_displayPassword(response)
		}
	});
}

function ptr0y_loadEncryptionKey(set) {
	if (set == false) {
		$("#ptr0y-rest-message").text("Please enter your encryption key.");
		ptr0y_activate('REST');
		$(".ptr0y_entries_card:not(.hidden)").slideUp();
		$("#ptr0y_encryption_key").attr("readonly",false).val("")
		$("#set_encryption_key").text("set").removeClass("reset")
	} else if (set == true) {
		$("#ptr0y-rest-message").text("Welcome to your password manager.");
		$(".ptr0y_entries_card").slideDown().removeClass("hidden");
		$("#ptr0y_encryption_key").attr("readonly",true)
		$("#set_encryption_key").text("reset").addClass("reset")
	}
}

function ptr0y_copyStringToClipboard (str) {
   var el = document.createElement('textarea');
   el.value = str;
   el.setAttribute('readonly', '');
   el.style = {position: 'absolute', left: '-9999px'};
   document.body.appendChild(el);
   el.select();
   document.execCommand('copy');
   document.body.removeChild(el);
}