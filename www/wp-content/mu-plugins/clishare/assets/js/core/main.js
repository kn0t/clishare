$(document).ready(function(){

	if (document.display_error === true) {
		$(".ptr0y-error").slideDown();
	}
	
	$("button[data-activate]").click(function(){
		view = $(this).data("activate");
		ptr0y_activate(view);
	});
	///////////////////////////////////////////
	$("#add_new_entry").click(function(){
		new_label = $("#new_entry_label").val();
		if (new_label != "" && typeof new_label != "undefined") {

			$.ajax({
				'url': ajax_url,
				'data': {'action': 'create_entry', 'entry_label': new_label},
				'type': 'post',
			})
			.done(function(response){
				if (response != 0 && response != "") {
					ptr0y_addNewEntryView(new_label, response)
					ptr0y_activate("REST");
					$("#new_entry_label").val("");
				}
			});
		}
	});
	///////////////////////////////////////////
	$(document).on("click", ".ptr0y_entry", function(){
		element = $(this);
		label =	$(this).data("label");
		enckey = $("#ptr0y_encryption_key").val();
		ptr0y_loadPassword(label,enckey);
		setTimeout(function(){
			ptr0y_updateEntry(element,label);
		},300);
	});
	///////////////////////////////////////////
	$("#ptr0y_copy_entry").click(function(){
		ptr0y_password = $(".ptr0y_password").data('clear');
		ptr0y_copyStringToClipboard(ptr0y_password);
	});
	///////////////////////////////////////////
	$("#ptr0y_view").click(function(){
		clear_password = $(".ptr0y_password").data("clear");
		$(".ptr0y_password").text(clear_password)
	});
	//////////////////////////////////////////
	ptr0y_loadEncryptionKey(false);
	$("#set_encryption_key").click(function(){
		if ($("#ptr0y_encryption_key").val() != "") {
			if( $(this).hasClass("reset")) {
				ptr0y_loadEncryptionKey(false);
			} else {
				ptr0y_loadEncryptionKey(true);
			}
		}
	});


});