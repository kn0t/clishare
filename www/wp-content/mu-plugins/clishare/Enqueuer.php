<?php

namespace Clishare;

class Enqueuer {

	private static $assets_base = "assets";
	public static $libraries = [
		"core"	=>	[
			"scripts"	=>	[
				"functions"	=>	"functions.js",
				"main"		=>	"main.js",
				"particles"	=>	"power.particles.js"
			]
		]
	];
	
	public static function load($view) {
		$plugin_dir 	=	plugin_dir_url(__FILE__);
		if (isset(self::$libraries[$view])) {
			$load	=	self::$libraries[$view];
			if (isset($load['scripts'])) {
				foreach ($load['scripts'] as $slug => $script) {
					$script_path 	=	$plugin_dir.self::$assets_base."/js/$view/$script";
					wp_enqueue_script($slug, $script_path, ["jquery"]);
				}
			}
			if (isset($load['styles'])) {
				foreach ($load['styles'] as $slug => $style) {
					$script_path 	=	$plugin_dir.self::$assets_base."/css/$view/$style";
					wp_enqueue_style($slug, $script_path);
				}
			}
		}
	}

}