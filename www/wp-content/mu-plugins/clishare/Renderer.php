<?php

namespace Clishare;

class Renderer extends Core {

	private $theme_location;
	private $context;
	
	public function __construct() {

		require_once TIMBER_AUTOLOAD;
		
		add_filter('timber_context', [$this,"globalAddToContext"]);

		$this->theme_location 	=	get_stylesheet_directory();
		$this->context 			=	\Timber::context();
	}

	public function globalAddToContext($context) {
		
		$context['production']	=	$this->is_production;
		$context['items']		=	Items::getItems();
		$context['logout_url']	=	wp_logout_url("/");

		return $context;
	}

	public function render($view, $title = false, $context = []) {
		if (is_array($context)) {
			$context 	=	array_merge($this->context, $context);
		} else {
			$context 	=	$this->context;
		}
		if($title) {
			$context['page_title']	=	$title;
		}
		$view =	$this->theme_location."/$view.twig";
		if (file_exists($view)) {
			print(\Timber::compile($view, $context));
		} else {
			die("Can't render file: $view");
		}
	}
}

?>