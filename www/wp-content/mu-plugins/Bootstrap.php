<?php
/*///////////////////////////////////////////////////////////////////
 * Plugin Name: Clishare
 * Version: 0.1
 * Plugin URI: https://eligent.group/
 * Description: Clishare Core Plugin
 * Author: Filippo kn0t Teodori
 * Author URI: https://eligent.group/
 * Requires at least: 4.0
 * Tested up to: 5.3
 */
/////////////////////////////////////////////////////////////////////

if (isset($_GET['dbgmode'])) {
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
}

// Declare debug function
function d($var, $kill = false) {
	print"<pre><code>";
	var_dump($var);
	print"</code></pre>";
	if ($kill === true) {
		die();
	}
}

/////////////////////////////////////////////////////////////////////
// Useful globals
define('SENDINBLUE_AUTOLOAD', dirname(__FILE__)."/sendinblue-lib/autoload.php");
define('TIMBER_AUTOLOAD', dirname(__FILE__)."/timber-library/timber.php");
define('JWT_AUTOLOAD', dirname(__FILE__)."/jwt/autoload.php");

// Instantiate core
require_once("clishare/clishare.php");
new Clishare\Core();

// Load Modules
require_once("clishare/JWT.php");
require_once("clishare/Emails.php");
require_once("clishare/Mongo.php");
require_once("clishare/Enqueuer.php");
require_once("clishare/Ajax.php");
require_once("clishare/Renderer.php");
require_once("clishare/Routes.php");
require_once("clishare/APIs.php");
require_once("clishare/Items.php");
require_once("clishare/Auth.php");

?>