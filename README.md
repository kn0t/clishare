# WP Starter Ting [0.9]

WPST is a package that allows to quickly spin up a WordPress VM in minutes. The box is based on Ubuntu Linux 18.04LTS and the package is going to setup the a LEMP installation with the following software:
| Software Stack |
| ------ |
| Ubuntu Linux 18.04LTS |
| nginx/1.14.0 |
| MySQL 5.7.24-0ubuntu0.18.04.1 |
| PHP-fpm 7.2 |

# Main Features

  - Sets up and automatically provides the box when needed 
  - Installs Nginx and loads the custom configuration file
  - Once the VM is ready, starts watching the SASS files for changes.

# Requirements
 - Git
 - Vagrant
     - Hostmanager (vagrant extension): ```vagrant plugin install vagrant-hostmanager```
 - VirtualBox / any other [suitable provider](https://www.vagrantup.com/docs/providers/)
 - Sass

# Configuration

Clone the repository into ```new-project```:
```sh
$ git clone https://bitbucket.org/kn0t/wp-starter-ting/src/master/ new-project
```

##### Checking Nginx configuration file:
At this stage is probably a good idea to immediately check nginx's configuration file (prior to the provision, as this configuration will be applied to the VM only after it has been initialised). To do so, the edit the file located at ```new-project/provision_files/nginx_vhost```.

# Ignition
Run the init file:
```sh
$ cd new-project
$ chmod +x init.sh
$ ./init.sh
```

# Provision Process
During the provision of the box, the following events occur in chronological order:
 - Creates a new user with credentials ```ubuntu : vagrant```
 -  Repositories are updated
 - Install: ```nginx```
 - Install: ```mysql-server``` with credentials ```root : 1337m4st3r```
 - Creates a database called ```wp```
 - Install: ```php-fpm, php-mysql, php-curl, curl, unzip``` (7.2)
 - Moves ```provision_files/nginx_vhost``` to ```/etc/nginx/sites-available/default``` applying the custom nginx configuration
 - Restarts ```nginx```
 - Checks if WordPress needs to be set up. If yes, sets up the latest version of WordPress in ```/var/www/public```

# Sass
Currently, once ```init.sh``` terminates, SASS is put in watch mode and updated every ```2s```. In order to compile the CSS manually, the syntax should look like the string below:
```
$ sass www/wp-content/themes/0x00/css/scss/style.scss:www/wp-content/themes/0x00/style.css
```

# Credits
 - Filippo <kn0t> Teodori - Eligent Group - filippo@eligent.group
     - Core Development
 - Richard Phelps - Eligent Group - richard@eligent.group
     - Saved the mu-plugins index file from Filippo's stupidity
     - Debug help

# License 
Copyleft - All Wrongs Reversed

