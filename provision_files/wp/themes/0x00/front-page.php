<?php
$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['posts'] = Timber::get_posts([
		"post_type" => "post",
		"post_status" => "publish",
		"posts_per_page" => 10,
		
	]);

Timber::render( array( "front-page.twig", 'page-' . $post->post_name . '.twig', 'page.twig' ), $context );